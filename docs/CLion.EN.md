# Development with CLion

## 1. Select and clone your fork with GitLab

![Select project from VCS](CLion/01-get-from-vcs.png)

## 2. In the project window, select `CMakeLists.txt ` on the left panel

![Load CMake project](CLion/02-load-cmake-project.png)

A hint will appear on top of the editor window, offering to upload CMake to the project. Click **`Load CMake project`**.

An error may appear in the build window, for example: `Unexpected build type MSan, possible values: Debug;Release;ASan;LSan;UBSan`.
It means that this configuration is not provided on your OS or with your compiler,
but it won't hurt to develop with other configurations.

##3. Select the required configuration from the drop-down list

![Choose Config](CLion/03-choose-configuration.png)

- **`Debug`** compiles quickly and is suitable for development.
- **`ASan, LSan, MSan, UBSan`** are suitable for debugging segmentation errors and other memory problems. Recommended
run your code with sanitizers before sending it for verification!
- **`Release`** is needed to build code with optimizations and check the execution speed.

Select **`All CTest`** as the build target. Now you can build a project and
run it through the buttons on the top right as usual.

If during the build you got an error like `C:\CLion 2022.2.4\bin\mingw\bin/ld.exe : cannot find -lasan`, which means you
don't have the right library to run this profile. You can just choose a different configuration.
