# Development with Visual Studio Code

Extensions are required for development:

- **`CMake`** from `twxs`
- **`CMakeTools`** from `Microsoft`
- **`C/C++`** from `Microsoft`

Restart VSCode after installing the extensions.

## 1. Select and clone your fork with GitLab

![Clone repository](VSCode/01-clone.png)

## 2. In the project window, select the compiler

![Choose Kit](VSCode/02-choose-kit.png)

VSCode will prompt you to select a compiler when you open the project. If he didn't,
click on the button with a wrench on the bottom panel.

On Windows, most likely, you need the `Visual Studio` compiler with the `amd64` version.

## 3. Select the configuration on the bottom panel

![Choose Config](VSCode/03-choose-config.png)

- **`Debug`** compiles quickly and is suitable for development.
- **`ASan, LSan, MSan, UBSan`** are suitable for debugging segmentation errors and other memory problems. Recommended
run your code with sanitizers before sending it for verification!
- **`Release`** is needed to build code with optimizations and check the execution speed.

On the same bottom panel, use the **`Build`** button to build the code and **`Run CTests tests`** to run the tests.

If during the build you see an error like `...\Microsoft.CppBuild.targets(457,5): error MSB8013: This project doesn't contain the Configuration and Platform combination of MSan|x64`, this means that the selected configuration on your system is not supported - choose another one.
