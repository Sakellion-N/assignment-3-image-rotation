# Development with Visual Studio

## 1. Select and clone your fork with GitLab

![Select project from VCS](VS/01-vcs.png)

## 2. In the solution window, select `Folder View` in the panel on the left

![Select Folder View](VS/02-folder-view.png)

Visual Studio will run the project configuration using CMake in the default profile (**`x64-Debug`**).

## 3. Select the required configuration from the drop-down list

![Choose Config](VS/03-choose-config.png)

- **`x64-Debug`** compiles quickly and is suitable for development.
- **`x64-Asan`** is suitable for debugging segmentation errors and other memory problems. Recommended
  to run your code in Asan before sending it for verification!
- **`x64-Release`** is needed to build code with optimizations and check the execution speed.

## 4. Use the panels on top to run the build and test

![Run build and tests](VS/04-tests.png)

- Build options are in the **`Build`** menu. To build the entire solution, press **`F7`**.
- To run the tests, select **`Run CTests for ...`** in the menu **`Test`**.
