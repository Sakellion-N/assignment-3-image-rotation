# Working with the terminal

## 1. Configuration

```bash
$ mkdir ./build/
$ cd ./build/
$ cmake .. -DCMAKE_BUILD_TYPE=<config>
```

Instead of `<config>`, you can use one of the existing configurations:

- **`Debug`** compiles quickly and is suitable for development.
- **`ASan, LSan, MSan, UBSan`** are suitable for debugging segmentation errors and other memory problems. Recommended
run your code with sanitizers before sending it for verification!
- **`Release`** is needed to build code with optimizations and check the execution speed.

## 2. Assembly

```bash
$ cmake --build ./build/
```

Executable files `./build/solution/image-transformer` and `./build/tester/image-matcher`
will be collected, they can be used for manual testing.

##3. Testing

```bash
$ cmake --build ./build/ --target check
# OR
$ cd ./build/
$ ctest --output-on-failure
```

## Bonus: `ssh` + `git`

### How to configure SSH keys

```bash
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
```

In the GitLab profile settings, you need to open the `SSH Keys` category, add a new
key and copy the contents of `id_rsa.pub` there.

### How to tilt a fork

```bash
$ git clone ssh://git@gitlab.se.ifmo.ru:4815/<my username>/assignment-image-rotation.git
$ cd ./assignment-image-rotation/
```

### How to send your changes back to the fork

```bash
$ git add ./solution/
$ git status
$ git commit -m "Lab complete"
$ git push origin master
```

After you open the merge request, every new change added in this way
will appear there automagically.

### How to update your lab if the teacher asked you to "pull up fresh changes" from the main repository

```bash
$ git remote add upstream ssh://git@gitlab.se.ifmo.ru:4815/programming-languages/assignment-image-rotation.git
$ git fetch upstream
$ git checkout master
$ git merge upstream/master
$ git remote remove upstream
```
