
- `BmpHeader` is small, only 60 bytes. You can not create it in the heap, but allocate it in the stack.

- `image` is small. You can not create it in the heap, but select it on the stack and return it by value.

- This function is needed only in one part of the program that works with BMP files. There is no point in making it accessible from the outside. It can only be defined in a .c file and marked as `static`.

- `padding` is counted multiple times in different parts of the program. You can make a separate function for this.

- Do not use bit operations to count padding. The compiler optimizes what is really safe to optimize.

- You can create a `header` from a `struct image`, then it will be correct immediately. It is good when not fully constructed objects do not appear in the code.

- There is no error handling in `fread`/`fwrite`.

- When declaring a structure as a *local variable*, all its fields contain garbage.
  It would be nice to specify the values of all its fields right here.
  Or at least initialize `= {0}`.

- It is reasonable to make a function that will read the address of a pixel by its coordinates.

- You can combine declaration and initialization.

- Numbers of different types are compared.

- An array instead of `switch` is more convenient for matching strings to variants of `enum`.

- Messages about how the program is executed &mdash; in `stderr`; its results &mdash; in `stdout`.

- The function is a candidate for `static`.

- As a rule, we name functions that work with structures using the prefix `struct_name`.

- "Magic numbers"

- These functions belong to another part of the program that works with the internal representation of the image, not with bmp files.

- Allocating memory for structures to `calloc` is not very correct. Better `malloc` + initialization with `={0}`.
Reason: "initialize with null values"!= "fill with zero bytes". Zero values for `float`/`double` do not necessarily have zeros in all bits. Null pointers `NULL` do not necessarily consist exclusively of null bytes (on some architectures, all bits may be set, i.e. -1).

- It is reasonable to separate header generation into a separate function.

- Checking the header for correctness is reasonable to allocate to a separate function.

- Any pointer is implicitly converted from `void*` to any other type; caste is not needed.

- Local variables that will not change, for example, which are created for convenience in order to write less, it is useful to mark `const`.

- The variable is declared outside the loop, but is used only inside. Why not combine initialization and declaration?

- Do not return an error string from the function. Reason: it is difficult to analyze. It is not obvious how to make a check in the calling function "and what exactly went wrong in `bmp_reader`?" and on the basis of it to do this or that action. We'll have to compare the lines, which is long. And error messages can be in different languages.
Therefore, enumerations are made to report the results of the function execution.

- Do not accept the BMP file header in the arguments of a function available for other parts of the program!
This forces the caller of this function to *generally know* about the headers of bmp files, and he does not need it. He just wants to make a .bmp file out of a picture.
Especially since the bmp header is generated from `image`.

- The transformation user does not want to *even know* about the headers of bmp files, he does not need it.  He just wants to make a .bmp file out of a picture.
Therefore, everything about the title should be hidden from him.
In view-header, a program was written to display the headers of bmp files, and not a program for rotating images.



- view-header is a utility for viewing the headers of bmp files, and not a blank for your program, for which bmp headers are *unimportant*.

- Ignore the remark about architecture, for example, declaring struct image in the same place as functions for working with a bmp file.
If we want extensibility and simplification of working with parts of the program, we need to allocate at least three structural parts of the program: an adapter for bmp files, a description of struct image and functions for working with it, and transformations over image. If you have definitions from more than one part within the same file, something is wrong.

- Memory was allocated before `return` in line XXX, now it must be released before `return` with the error code.


- For example, let's take:

```
uint8_t value = 0;

fwrite( &value, padding, 1, file);
```


Here `fwrite` writes bytes in a row to the `padding * 1` file starting from the address `&value`.


- Such small functions that are used only in one file should be marked `static`. Then not only can they be embedded in the call locations, but the compiler can remove their definition from the file altogether.

- Do not mix `camelCase` and `snake_case` in one project.
