# Assignment: Image rotation
---
Laboratory work: Image rotation

# Preparation

- Read chapter 12 (pp. 221, 231&ndash;239) and 13 (in its entirety) "Low-level programming: C, assembly and program execution".

On defense, we can discuss any questions from the textbook from chapters 8&ndash;13 inclusive.

# BMP file structure

The BMP file consists of a header and a raster array.
The header is set by the following structure (note the `packed` attribute):

```c
// Description for gcc and clang
#include  <stdint.h>
struct bmp_header __attribute__((packed))
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
```

Immediately after it (is it always?) there is a raster array in which pixels are stored sequentially by lines.
Each pixel is defined by a 3 byte structure:

```c
   struct pixel { uint8_t b, g, r; };
```

## Padding

If the width of the image in bytes is a multiple of four, then the lines go one after the other without gaps.
If the width is not a multiple of four, then it is padded with garbage bytes to the nearest multiple of four.
These bytes are called *padding*.

Example:

1. The image has a width of 12 pixels = 12 * 3 bytes = 36 bytes. The width is a multiple of four, each next line begins immediately after the previous one.
2. The image has a width of 5 pixels. 5 * 3 = 15 bytes, the nearest multiple of four (rounding up) this is 16. After each line there will be an indent of 16-15 = 1 byte before the start of the next one.

Please note: the margins are in *bytes*, not in pixels.



# To users of the compiler from Microsoft

You will have to set the structure differently, without the `packed` attribute:

```c
#include  <stdint.h>
#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)
```

The explanation of this read is on pages 235&ndash;239 of the textbook.


# About architecture

The program is divided into modules; each module is a `.c` file, which becomes a file with the extension `.o`.

A well-thought-out application architecture in each specific module minimizes
knowledge about other modules for the following reasons:

- When a programmer works on one module (develops, modifies,
  looks for errors), it is easier for him not to keep in mind knowledge about the rest
  the program. Hiding information about other modules allows him not to take in
  head details of their internal structure.
- Let module A not use definitions from module B, but have access to them.
  Of course, you can change B any way you want and it won't affect A.
  However, there is a chance that the author of the program or one of the future co-authors may
  use definitions from B in module A, because there is access to them. This
  it will establish a tight connection between A and B, and it will no longer be possible to freely change B without
  affecting A. Programs are complex systems, and we want to have a minimum
  of connections between their elements, otherwise modification (and error correction) will be
  require constant modification of not one, but many parts of the program.

In our case, it is reasonable to allocate several parts in the program.

## Part 1: Internal Format

Description of the internal representation of the image `struct image`, cleared of
format details, and functions for working with it: creation, deinitialization, etc.

   ```c
   struct image {
     uint64_t width, height;
     struct pixel* data;
   };
   ```
 
  This part of the program should not know about either input formats or transformations.

## Part 2: Input formats

Each input format is described in a separate module; they provide functions
for reading files of different formats in `struct image` and for writing to disk in
the same formats.

These modules know about the module describing the `struct image`, but they don't know anything about
transformations. Therefore, it will be possible to add new transformations without rewriting
the code for the input formats.

As soon as we read the image into the internal format, we have to forget
which format it was read from! That is why
only the minimum of image details (dimensions) are left in the `struct image`, and no parts
bmp header. For BMP, you can start with:

```c
/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

```

The `from_bmp` and `to_bmp` functions accept an already open file, which allows
them to work with pre-opened files `stdin`, `stdout`, `stderr`.

The `from_bmp` and `to_bmp` functions should neither open nor close files.
For open/close errors, you may want to introduce separate
enumeration types.

As soon as we read the image into the internal format, we have to forget
which format it was read from! That is why
only the minimum of image details (dimensions) are left in the `struct image`, and no parts
bmp header.

You will also need functions similar to `from_bmp` and `to_bmp`, which
will accept file names and deal with the correct opening (`fopen`) and
closing (`fclose`) of files; on open files they can run `from_bmp`
and `to_bmp`.

It makes sense to separate opening/closing files and working with them. Already
opening and closing may be accompanied by errors (see `man fopen` and
`man fclose`) and I want to separate open/close error handling and
read/write error handling.



## Part 3: Transformations

Each transformation is described in a separate module. These modules know about
the module describing the `struct image`, but they don't know anything about the input formats.
Therefore, it will be possible to add new input formats without rewriting the code for
transformations. Without additional effort, we will get the opportunity by describing the input
format, immediately support all transformations over it.

You will need a function to rotate the image in its internal representation:

   ```c
   /* creates a copy of the image that is rotated 90 degrees */
   struct image rotate( struct image const source );
   ```

You may need [a program for displaying BMP file headers](https://gitlab.se.ifmo.ru/low-level-programming/bmp-header-viewer )
**ATTENTION** This is a program for viewing BMP file headers. This is not a blank for a solution! You can compile it using `make` and check the headers for bitness; in the solution you need to support only 24-bit BMP files.

## Part 4: Everything else

The rest of the program can be organized in any meaningful way. You may want to write a small library for I/O, working with strings, etc.

Reasonable creation of new modules and the introduction of additional functions for convenience, where necessary, is welcome.

Additional functions that you have introduced for convenience, but which do not relate in meaning to any of these modules, can be separated
into a separate module. It is often called `util.c` or something similar.

# Task


- It is necessary to realize the rotation of the image in BMP format by 90 degrees against
  clockwise. The format of use is as follows:

```
./image-transformer <source-image> <transformed-image>
```

- The architecture of the application is described in the previous section.
- The code is located in the `solution/src` directory, header files are searched in `solution/include`.

## Build and test system

To build the code, you are provided with a CMake build system, you do not need to write the build system yourself.

- Depending on the platform and compiler, the build system supports several configurations with dynamic
analyzers (sanitizers). Sanitizers can give detailed information about possible and real errors in
the program instead of the classic segmentation fault message. You can choose the appropriate configuration with
using the `CMAKE_BUILD_TYPE` variable:

- `ASan` &mdash; [AddressSanitizer](https://clang.llvm.org/docs/AddressSanitizer.html),
  a set of checks for incorrect use of memory addresses. Examples:
  use-after-free, double-free, going beyond the stack, heap, or static block.

- `LSan` &mdash; [LeakSanitizer](https://clang.llvm.org/docs/LeakSanitizer.html ),
  checks for memory leaks.

- `MSan` &mdash; [MemorySanitizer](https://clang.llvm.org/docs/MemorySanitizer.html ),
  checks that any used memory cell is initialized at the time of reading from it.

- `UBSan` &mdash; [UndefinedBehaviourSanitizer](https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html),
  a set of basic checks for undefined behavior. Examples: numerical type overflow,
  null-pointer dereference.

- If your system has a static analyzer `clang-tidy`, it will be launched during the compilation of the program.
  The list of checks is described in the file `clang-tidy-checks.txt `. You can add your checks to the end of this file.

- The `tester` directory contains code and images for testing your program. CTest is used to run the tests.

- The integration of the build system with the CLion, Visual Studio and Visual Studio Code development environments is supported.

In order for the build system to work on your system, you need:

### Linux and macOS

- Compiler (`gcc`/`clang') and 'cmake' (check that `cmake' version 3.12 or higher)
- If you want to use sanitizers with GCC, install `libasan`, `liblsan` and `libubsan` using the package manager (the names may differ).
- If you want to use sanitizers with Clang, on some systems you may need the `compiler-rt` package.
- If you want to use 'clang-tidy', install `clang-tools-extra`.

### Windows

- Any development environment (CLion, Visual Studio, Visual Studio Code)
- If you want to use `clang-tidy`, download LLVM: https://github.com/llvm/llvm-project/releases (find the win64 installer under one of the versions)
- For VS Code, you need to install Visual Studio (from the Microsoft website) and CMake separately: https://cmake.org/download/

### Assembly and testing instructions

- [Working with Terminal](docs/Terminal.md)
- [CLion](docs/CLion.md)
- [Visual Studio](docs/Visual%20Studio.md)
- [Visual Studio Code](docs/VSCode.md)

# For self-checking

- Read [rules of good style](https://gitlab.se.ifmo.ru/c-language/main/-/wikis/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0-%D1%81%D1%82%D0%B8%D0%BB%D1%8F-%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D1%8F-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC-%D0%BD%D0%B0-C). Your solution must match them.
- Architecture: Think about how you would like to organize the code to easily add input formats (not just BMP) and transformations (not just 90 degree rotation).
- Please send the solution in the form of a pull-request. [Instruction](https://gitlab.se.ifmo.ru/cse/main/-/wikis/%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D1%81%D0%BB%D0%B0%D1%82%D1%8C-%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0-%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D1%83).
