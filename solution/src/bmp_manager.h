#pragma once

#include "bmp_structure.h"

void read_header(FILE* file, struct BmpHeader* header);

uint8_t** read_bitmap(FILE* file, size_t height, size_t length);

struct BMP read_bmp(FILE* file);

void write_header(FILE* file, const struct BmpHeader* header);

void write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width);

void write_bmp(FILE* file, const struct BMP* image);
