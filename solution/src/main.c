#include "image_rotation.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Invalid amount of arguments: %i", argc);
        return -1;
    }
    FILE* input = fopen(argv[1], "rb");
    FILE* output = fopen(argv[2], "wb");
    if (input == NULL || output == NULL) {
        printf("File doesn't exists!");
        return -1;
    }

    rotate_image(input, output);

    fclose(input);
    fclose(output);

    return 0;
}
