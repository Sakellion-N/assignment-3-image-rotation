#pragma once

#include "bmp_structure.h"

struct BMP get_rotated(FILE* input);

void rotate_image(FILE* input, FILE* output);
