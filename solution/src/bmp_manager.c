#include "bmp_manager.h"

#include <stdlib.h>

void read_header(FILE* file, struct BmpHeader* header) {
    fread(header, 1, BMP_HEADER_SIZE, file);
}

uint8_t** read_bitmap(FILE* file, size_t height, size_t length) {
    uint8_t** bitmap = malloc(sizeof(uint8_t*) * height);
    size_t padding_length = 0;
    uint8_t* padding_buffer = NULL;
    if (length % 4 != 0) {
        padding_length = 4 - (length % 4);
        padding_buffer = malloc(padding_length);
    }
    for (int i = 0; i < height; ++i) {
        bitmap[i] = malloc(length);
        fread(bitmap[i], 1, length, file);
        if (padding_buffer != NULL) {
            fread(padding_buffer, 1, padding_length, file); // read padding to skip it
        }
    }
    if (padding_buffer != NULL) {
        free(padding_buffer);
    }
    return bitmap;
}

struct BMP read_bmp(FILE* file) {
    struct BMP bmp;
    read_header(file, &bmp.bmp_header);
    // read color table if it presents
    if (bmp.bmp_header.bOffBits != BMP_HEADER_SIZE) {
        bmp.color_table = malloc(bmp.bmp_header.bOffBits - BMP_HEADER_SIZE);
        fread(bmp.color_table, 1, bmp.bmp_header.bOffBits - BMP_HEADER_SIZE, file);
    } else {
        bmp.color_table = NULL;
    }
    // reading image, where byte-length equals color_byte * width in pixels
    bmp.image = read_bitmap(file, bmp.bmp_header.biHeight,
                            ((size_t) bmp.bmp_header.biBitCount / 8) * (size_t) bmp.bmp_header.biWidth);
    return bmp;
}

void write_header(FILE* file, const struct BmpHeader* header) {
    fwrite(header, BMP_HEADER_SIZE, 1, file);
}

void write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width) {
    size_t padding_length = 0;
    uint8_t* padding = NULL; // preparing padding
    if (width % 4 != 0) {
        padding_length = 4 - width % 4;
        padding = malloc(padding_length);
        for (int i = 0; i < padding_length; ++i) {
            padding[i] = 0;
        }
    }
    for (size_t i = 0; i < height; ++i) {
        fwrite(data[i], 1, width, file);
        if (padding_length != 0) {
            fwrite(padding, 1, padding_length, file);
        }
    }
    if (padding != NULL) {
        free(padding);
    }
}

void write_bmp(FILE* file, const struct BMP* image) {
    write_header(file, &(image->bmp_header));
    if (image->color_table != NULL) {
        fwrite(image->color_table, 1, image->bmp_header.bOffBits - BMP_HEADER_SIZE, file);
    }
    write_bitmap(file, (const uint8_t**) image->image, image->bmp_header.biHeight,
                 image->bmp_header.biWidth * (image->bmp_header.biBitCount / 8));
}
